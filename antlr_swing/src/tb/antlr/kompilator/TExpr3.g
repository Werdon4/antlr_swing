tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
  Integer scp_depth = 0;
  Integer if_counter = 0;
  Integer loop_counter = 0;
}

prog    : (e+=zakres | e+=expr | d+=decl)* -> program(name={$e},deklaracje={$d})
;

zakres : ^(BEGIN {scp_depth=enterScope();} (e+=zakres | e+=expr | d+=decl)* END) {scp_depth=leaveScope();} -> blok(wyr={$e}, dekl={$d})
;

decl  :
        ^(VAR i1=ID) {locals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> sub(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> mul(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> div(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) -> write(p1={$e1.st}, name={getNameWithId($i1.text)})
        | INT  {numer++;}          -> int(i={$INT.text},j={numer.toString()})
        | ID {isInScope($ID.text)}? -> read(n={getNameWithId($ID.text)})
        | ^(IF e1=expr e2=expr e3=expr?) {++if_counter;} -> if(e1={$e1.st}, e2={$e2.st}, e3={$e3.st}, id={if_counter.toString()})
        | ^(WHILE e1=expr e2=expr) {++loop_counter;} -> loop(e1={$e1.st}, e2={$e2.st}, id={loop_counter.toString()})
    ;
    