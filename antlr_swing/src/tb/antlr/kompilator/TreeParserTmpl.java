/**
 * 
 */
package tb.antlr.kompilator;

import org.antlr.runtime.RecognizerSharedState;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.TreeNodeStream;
import org.antlr.runtime.tree.TreeParser;

import tb.antlr.symbolTable.GlobalSymbols;
import tb.antlr.symbolTable.LocalSymbols;

/**
 * @author tb
 *
 */
public class TreeParserTmpl extends TreeParser {

	protected GlobalSymbols globals = new GlobalSymbols();
	protected LocalSymbols locals = new LocalSymbols();
	
	/**
	 * @param input
	 */
	public TreeParserTmpl(TreeNodeStream input) {
		super(input);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param input
	 * @param state
	 */
	public TreeParserTmpl(TreeNodeStream input, RecognizerSharedState state) {
		super(input, state);
		// TODO Auto-generated constructor stub
	}

	protected void errorID(RuntimeException ex, CommonTree id) {
		System.err.println(ex.getMessage() + " in line " + id.getLine());
	}
	
	protected Integer enterScope() {
		return locals.enterScope();
	}
	
	protected Integer leaveScope() {
		return locals.leaveScope();
	}

	protected boolean isInScope(String name) {
		if(locals.hasSymbol(name)) {
			return true;
		} else {
			System.out.println("Variable " + name +" do not exists in this scope!");
			return false;
//			throw new RuntimeException("Variable " + name +" do not exists in this scope!");
		}
	}
	
	String getNameWithId(String name) {
		if(locals.hasSymbol(name)) {
			Integer depth = locals.getSymbolDepth(name);
			return name +  "_" + depth.toString();
		}else {
			throw new RuntimeException("Variable " + name +" do not exists in this scope!");
		}
	}
	
}
