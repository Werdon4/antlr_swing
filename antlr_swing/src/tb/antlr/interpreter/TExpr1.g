tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
import java.util.HashMap;
import tb.antlr.symbolTable.LocalSymbols;
}

@members {
LocalSymbols locals = new LocalSymbols();
}


prog : (block | expr | print | decl)*;
        
block : (
        ^(LBB {locals.enterScope();})
        (block | expr | print | decl)*
        ^(RBB {locals.leaveScope();})
       );
      
print : 
        ^(PRINT e=expr) {drukuj ($e.text + " = " + $e.out.toString());}
        ;
        
decl :   ^(VAR i1=ID) {locals.newSymbol($i1.text);}
        | ^(PODST i1=ID e=expr) {locals.setSymbol($i1.text, $e.out);} 
      ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = $e1.out / $e2.out;}
        | ^(MOD   e1=expr e2=expr) {$out = $e1.out \% $e2.out;}
        | ^(POW   e1=expr e2=expr) {
          int result = $e1.out;
          for(int i = 0; i < $e2.out - 1; i++){
            result*=$e1.out;
          }
          $out = result;
        }
        | ID                       {$out = locals.getSymbol($ID.text);}
        | INT                      {$out = getInt($INT.text);}
        ;
