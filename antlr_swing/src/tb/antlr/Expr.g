grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

tokens {FUNC; FUNC_BODY; FUNC_DECL;}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

//gramatyka

prog : (stat | block )+ 
    EOF!;

block
    : BEGIN^ (stat | block )* END!
//    NL!
    ;

stat
    : expr NL -> expr

    | VAR ID (PODST expr)? NL -> ^(VAR ID) ^(PODST ID expr)?
//    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    | PRINT expr NL -> ^(PRINT expr)
    | if_stat NL -> if_stat
    | loop_stat NL -> loop_stat
    | NL ->
    ;
    
if_stat
    : IF^ expr THEN! (expr) (ELSE! (expr))?
    ;

loop_stat
    : WHILE^ expr DO! expr 
    ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      | MOD^ atom
      | POW^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

//func_decl
//    : FUNC ID LP (parameters)? RP BEGIN func_body END (FUNC_DECL^ ID func_body)
//    ;

parameters
    :expr (',' expr)*
    ;
    
func_body
    : prog;

//leksemy

WHILE : 'while';

DO : 'do';

//GREATER : '>';
//
//LESSER : '<';
//
//EQUAL : '==';

IF : 'if';

ELSE : 'else';

THEN : 'then';

FUNC : 'func';

VAR :'var';

PRINT : 'print';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

//NL : '\r'? '\n' ;
NL : ';' ;

WS : (' ' | '\t' | '\n')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
	
MOD
  : '%'
  ;
  
POW
  : '**'
  ;
  
BEGIN
  : '{'
  ;
 
END
  : '}'
  ;
